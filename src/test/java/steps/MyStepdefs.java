package steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import database.model.Customer;
import database.repository.CustomerDao;
import database.repository.CustomerRepository;
import org.junit.After;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import pageobjects.ContactFormPage;
import pageobjects.HomePage;

import java.util.List;
import java.util.Random;

@Scope("cucumber-glue")
public class MyStepdefs extends TestRunner {

    @Autowired
    private WebDriver driver;

    @Value("${browser.url:}")
    private String homepageUrl;

    private Object currentPage;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private CustomerRepository customerRepository;

    @Before
    public void setupCustomer() {
        long count = customerRepository.count();
        Assert.assertEquals(count, customerDao.getCustomers().size());

        Customer mdima = new Customer();
        mdima.setName("Marius Dima");
        mdima.setEmail("marius.dima@ing.com");
        createCustomerWithJPARepository(mdima);

        createCustomerWithJdbcDao(generateRandomCustomer());
        createCustomerWithJPARepository(generateRandomCustomer());

        //retrieve customer with email using native query
        customerRepository.findByEmailStartingWith("testschool");

        //retrieve customer with email using native query
        String expectedEmail = "marius.dima@ing.com";
        Customer customerWithEmailUsingNativeQuery = customerRepository.findCustomerWithEmailUsingNativeQuery(expectedEmail);
        Assert.assertEquals(expectedEmail, customerWithEmailUsingNativeQuery.getEmail());

        Customer customerWithEmailUsingQuery = customerRepository.findCustomerWithEmailUsingQuery(expectedEmail);
        Assert.assertEquals(expectedEmail, customerWithEmailUsingQuery.getEmail());

        Customer customerByEmail = customerRepository.findByEmail(expectedEmail);
        List<Customer> customers = customerDao.getCustomers();
        Assert.assertTrue(customers.contains(customerByEmail));
    }

    private Customer generateRandomCustomer() {
        Customer customer = new Customer();
        int random = new Random().nextInt(100);
        customer.setName("Test School" + random);
        customer.setEmail("testschool" + random + "@gmail.com");
        return customer;
    }

    private void createCustomerWithJdbcDao(Customer customer) {
        customerDao.create(customer);
    }


    private void createCustomerWithJPARepository(Customer customer) {
        customerRepository.save(customer);
    }


    @After
    public void cleanUpCustomers() {
        customerRepository.deleteAll();
    }

    @Given("I navigate to {string}")
    public void iNavigateTo(String url) {
        driver.get(homepageUrl);
        currentPage = new HomePage(driver);
    }

    @When("^I click on contact link$")
    public void iClickOnContactLink() {
        currentPage = ((HomePage) currentPage).clickContactLink();
    }

    @And("^I complete contact details$")
    public void iCompleteContactDetails() {
        ContactFormPage contactFormPage = (ContactFormPage) currentPage;
        contactFormPage.selectSubjectHeading("Customer service");
        contactFormPage.inputEmail(customerRepository.findByEmailStartingWith("testschool").get(0).getEmail());
        contactFormPage.inputMessage(" My Message");
    }

    @And("^I submit contact form$")
    public void iSubmitContactForm() {
        ((ContactFormPage) currentPage).clickSubmit();
    }

    @Then("^I get a contact form submitted$")
    public void iGetAContactFormSubmitted() {
        driver.quit();
    }


}
