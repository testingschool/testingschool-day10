package steps;

import backpack.TestBackpack;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import json.model.Data;
import json.model.LoginUserRequest;
import json.model.RegisterUserRequest;
import json.model.User;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

import static io.restassured.RestAssured.given;

/**
 * Created by Marius Dima on 05.09.2019.
 */
public class ApiSteps {

    private List<User> users;

    @Autowired
    private TestBackpack testBackpack;

    @Given("I list all users from {string}")
    public void iListAllUsersFrom(String url) {
        Data data = given().get(url).as(Data.class);
        users = data.getUsers();
    }

    @And("I save a random user email")
    public void iSaveARandomUserEmail() {
        int index = new Random().nextInt(users.size());
        User randomUser = users.get(index);
        testBackpack.setEmail(randomUser.getEmail());
    }


    @When("I register a new user at {string}")
    public void iRegisterANewUserAt(String url) {
        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setEmail(testBackpack.getEmail());
        String password = "pwd1234";
        testBackpack.setPassword(password);
        registerUserRequest.setPassword(password);

        String registerToken = given().contentType("application/json")
                .body(registerUserRequest)
                .post(url)
                .jsonPath().getString("token");
        testBackpack.setRegisterToken(registerToken);
    }

    @And("I login with the newly created user at {string}")
    public void iLoginWithTheNewlyCreatedUserAt(String loginUrl) {
        LoginUserRequest loginUserRequest = new LoginUserRequest();
        loginUserRequest.setEmail(testBackpack.getEmail());
        loginUserRequest.setPassword(testBackpack.getPassword());
        String loginToken = given().contentType("application/json")
                .body(loginUserRequest)
                .post(loginUrl)
                .jsonPath().getString("token");
        testBackpack.setLoginToken(loginToken);
    }

    @Then("The resulted token from login should be the same with the register token")
    public void theResultedTokenFromLoginShouldBeTheSameWithTheRegisterToken() {
        String registerToken = testBackpack.getRegisterToken();
        String loginToken = testBackpack.getLoginToken();
        Assert.assertEquals(registerToken, loginToken);
    }
}
