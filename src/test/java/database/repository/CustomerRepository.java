package database.repository;

import database.model.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Marius Dima on 12.09.2019.
 */
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    Customer findByEmail(String email);

    List<Customer> findByEmailStartingWith(String email);

    @Query(value = "from customer")
    List<Customer> listAllUsingQuery();

    @Query(value = "from customer where email = ?1")
    Customer findCustomerWithEmailUsingQuery(String email);

    @Query(value = "Select * from customer where email = ?", nativeQuery = true)
    Customer findCustomerWithEmailUsingNativeQuery(String email);
}
