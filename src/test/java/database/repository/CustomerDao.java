package database.repository;

import database.model.Customer;

import java.util.List;

/**
 * Created by Marius Dima on 12.09.2019.
 */
public interface CustomerDao {

    List<Customer> getCustomers();

    void create(Customer customer);

}
