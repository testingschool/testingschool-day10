package database.repository;

import database.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Marius Dima on 12.09.2019.
 */
@Component
public class CustomerDaoImpl implements CustomerDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public List<Customer> getCustomers() {
        String sql = "select * from customer";
        return jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(Customer.class));
    }

    @Override
    public void create(Customer customer) {
        String sql = "insert into Customer (name, email) values (?, ?)";
        jdbcTemplate.update(sql, customer.getName(), customer.getEmail());
    }
}
